import numpy as np
import pandas as pd
import datetime as dt

def goods_data_update_check(ipath):
    df = pd.read_csv(ipath)
    del df['config']
    df.iloc[:,0] = pd.to_datetime(df.iloc[:,0])
    
    #delete columns with all nans
    legal_cols = df.isnull().all().map(lambda x: not x)
    df = df.loc[:, legal_cols]
    
    results = pd.DataFrame(np.zeros((df.shape[1]-1, 5)), columns = ['cols', 'last_update_date', 'since_today', 'last_update_value', 'skewness'])
    
    print ('----- adding cols -----')
    cols = df.columns[1:]
    results['cols'] = cols
    #print (results)
    
    print ('----- adding last_update_date -----')
    tmp = df.apply(lambda col: col.last_valid_index(), axis = 0)
    tmp  = df.iloc[tmp[1:], 0]
    tmp = tmp.reset_index(drop = True)
    results['last_update_date'] = tmp
    #print (results)
    
    print ('----- adding since today -----')
    since_today = pd.to_datetime('today') - results['last_update_date']
    since_today = since_today.dt.days
    results['since_today'] = since_today
    #print (results)
    
    print ('----- adding last update value -----')
    last_update_value = df.ffill(axis = 0).iloc[-1, 1:]
    last_update_value = last_update_value.reset_index(drop = True)
    results['last_update_value'] = last_update_value
    #print (results)
    
    #df['cache.ts.csv'] = df['cache.ts.csv'].dt.strftime('%Y-%m-%d')
    #years = results['last_update_date'].dt.year
    #tmp = df.groupby(df['cache.ts.csv'].str[:4]).mean()
    #skewness = np.zeros(len(years))
    #cols = df.columns[1:]
    #for i in range(len(years)):
    #    skewness[i] = tmp.loc[str(years[i]), cols[i]]
    #print (skewness)
    #results['skewness'] = results['last_update_value'] /  skewness
    
    print ('----- adding skewness -----')
    series_len = df.shape[1] - 1
    skewness = np.zeros(series_len)
    date_bound = results['last_update_date'] - pd.Timedelta(days=365)
    for i in range(series_len):
        
        tmp = df.iloc[:, [0,i+1]]
        tmp = tmp[tmp.iloc[:,0] > date_bound[i]]
        #if i == 2779:
        #    tmp.to_csv('./hahah')
        tmp_mean = tmp.iloc[:,1].mean()
        skewness[i] = results['last_update_value'][i] / tmp_mean
    results['skewness'] = skewness
    #print (results)
    
    print ('----- writing -----')
    results.to_csv('./output/data_check.data', index = False)
    
    print ('----- done -----')

if __name__ == '__main__':
    ipath = '/home/luochen/baogang/check/cache.ts.csv'
    
    goods_data_update_check(ipath)
