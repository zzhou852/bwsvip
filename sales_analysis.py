import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression

def filter_df(df):
    df = df[df['SHOPSIGN'] != ' ']
    df = df[df['DELIVERY_DATE_CHR'] > '2016-01-01']
    df = df[((df['ACT_UNITPRICE'] == 0) & (df['ORDER_WT'] > 0)) == False]
    return df
    
def get_amount_increase(lr):
    ret = 0.0
    pred_begin = lr.predict(0)
    pred_now = lr.predict((pd.Timestamp.now() - pd.Timestamp(2016,1,1)).days)
    ret = pred_now[0] - pred_begin[0]
    return ret

def get_price_trend(df):
    prodIDs_df = df[['SHOPSIGN','WIDTH']].drop_duplicates()
    trend_component = pd.DataFrame(columns = ('SHOPSIGN', 'WIDTH', 'coef', 'intercept', 'amt_inc'))
    trend_histogram = []
    idx = -1
    for index, row in prodIDs_df.iterrows():

        idx += 1
        if (idx % 100) == 0:
            print ('processed ', idx)

        record = {}
        shopsign = row['SHOPSIGN']
        width = row['WIDTH']

        prod_df = df[(df['SHOPSIGN'] == shopsign) & (df['WIDTH'] == width)]
        X = pd.to_datetime(prod_df['DELIVERY_DATE_CHR']) - pd.Timestamp(2016,1,1)
        X = X.map(lambda x:x.days)
        X = np.array(X)
        X = X[:, np.newaxis]
        Y = np.array(prod_df['ACT_UNITPRICE'])
        lr = LinearRegression()
        lr.fit(X,Y)
        
        record['SHOPSIGN'] = shopsign
        record['WIDTH'] = width
        record['coef'] = lr.coef_[0]
        record['intercept'] = lr.intercept_
        record['amt_inc'] = get_amount_increase(lr)
        trend_component = trend_component.append(record, ignore_index = True)   
        
        trend_histogram.append(record['amt_inc'])
        
    trend_component.to_csv('./output/trend_component.csv')
 
    trend_histogram = sorted(trend_histogram)
    trend_histogram = [str(i) for i in trend_histogram]
    trend_histogram = ','.join(trend_histogram)
    fout = open('./output/trend_histogram.csv', 'w')
    fout.write(trend_histogram + '\n')
    fout.close()

def get_orderwt_trend(df):
    prodIDs_df = df[['SHOPSIGN','WIDTH']].drop_duplicates()
    trend_component = pd.DataFrame(columns = ('SHOPSIGN', 'WIDTH', 'coef', 'intercept', 'amt_inc'))
    trend_histogram = []
    idx = -1
    for index, row in prodIDs_df.iterrows():

        idx += 1
        if (idx % 100) == 0:
            print ('processed ', idx)

        record = {}
        shopsign = row['SHOPSIGN']
        width = row['WIDTH']

        prod_df = df[(df['SHOPSIGN'] == shopsign) & (df['WIDTH'] == width)]
        X = pd.to_datetime(prod_df['DELIVERY_DATE_CHR']) - pd.Timestamp(2016,1,1)
        X = X.map(lambda x:x.days)
        X = np.array(X)
        X = X[:, np.newaxis]
        Y = np.array(prod_df['ORDER_WT'])
        lr = LinearRegression()
        lr.fit(X,Y)
        
        record['SHOPSIGN'] = shopsign
        record['WIDTH'] = width
        record['coef'] = lr.coef_[0]
        record['intercept'] = lr.intercept_
        record['amt_inc'] = get_amount_increase(lr)
        trend_component = trend_component.append(record, ignore_index = True)   
        
        trend_histogram.append(record['amt_inc'])
        
    trend_component.to_csv('./output/prod_orderwt_trend_component.csv')
 
    trend_histogram = sorted(trend_histogram)
    trend_histogram = [str(i) for i in trend_histogram]
    trend_histogram = ','.join(trend_histogram)
    fout = open('./output/prod_orderwt_trend_histogram.csv', 'w')
    fout.write(trend_histogram + '\n')
    fout.close()


def get_sale_distribution(df):
    prod_sales_distribution = df[['SHOPSIGN', 'WIDTH', 'ORDER_WT']].groupby(['SHOPSIGN', 'WIDTH']).sum()
    prod_sales_distribution.to_csv('./output/prod_sales_distribution.csv')
    customer_concerntration = df[['SHOPSIGN', 'WIDTH', 'DIRECT_USER_NUM']].groupby(['SHOPSIGN', 'WIDTH']).sum() / df['DIRECT_USER_NUM'].nunique()
    customer_concerntration.to_csv('./output/customer_concerntration.csv')

def normalize(x):
    x_min = x.min()
    x_max = x.max()
    return (x - x_min) / (x_max - x_min)

def get_price_fluctuation(df):
    grp = df[['SHOPSIGN', 'WIDTH', 'ACT_UNITPRICE']].groupby(['SHOPSIGN','WIDTH'])
    prod_price_var = grp.var()
    prod_price_var.to_csv('./output/prod_price_var.csv')
    prod_price_var_normed = grp.apply(normalize)
    prod_price_var_normed.to_csv('./output/prod_price_var_normed.csv')

def get_transaction_freq(df):
    grp = df.groupby(['SHOPSIGN','WIDTH'])
    prod_transaction_freq = grp.size()
    prod_transaction_freq.to_csv('./output/prod_transaction_freq.csv')

def get_orderwt_fluctuation(df):
    grp = df[['SHOPSIGN', 'WIDTH', 'ORDER_WT']].groupby(['SHOPSIGN','WIDTH'])
    prod_orderwt_var = grp.var()
    prod_orderwt_var.to_csv('./output/prod_orderwt_var.csv')
    prod_orderwt_var_normed = grp.apply(normalize)
    prod_orderwt_var_normed.to_csv('./output/prod_orderwt_var_normed.csv')

if __name__ == '__main__':
    
    categorical_col = [
            'ORDER_TYPE','ORDER_TYPE_CODE', 'PROD_CODE', 'COMPANY_CODE',
            'ORDER_STATUS_CODE', 'CONTRACT_STATUS','FACTORY_ID',
            'SHOPSIGN','SALE_ORG','STEEL_TYPE'
            ]
    date_col = ['DELIVERY_DATE_CHR']
    continual_col = ['SIZE_DESC', 'WIDTH']

    df = pd.read_csv('/Users/luochen/Documents/jiachen/baosteel/baosteel_data/baosteel_data_fixed.csv')
    df = filter_df(df)
    
    #get_sale_distribution(df)
    #get_price_fluctuation(df)
    #get_price_trend(df)
    #get_transaction_freq(df)
    #get_orderwt_fluctuation(df)
    get_orderwt_trend(df)
    print ('done')
    


