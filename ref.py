# -*- coding: utf-8 -*-

#########################################################################
# FileName: ref.py
# Author: ziye
# CreatedTime: Sun Feb 11 11:29:13 2018
#########################################################################

import pandas as pd

def load(fpath, datenum=8):
    """ Load raw data. """
    df = pd.read_csv(fpath, index_col=0,
            low_memory=False,
            dtype={'DELIVERY_DATE_CHR': 'str',
                   'CONTRACT_STATUS': 'str',
                   'CONTRACT_NUM': 'str',
                   'FACTORY_ID': 'str',
                   'DIRECT_USER_NAME': 'str',
                   'SHOPSIGN': 'str',
                   'STEEL_TYPE': 'str',
                   'IS_PROD_CODE': 'str',
                  })
    df = df[df.DELIVERY_DATE_CHR.notnull()]
    df.DELIVERY_DATE_CHR = df.DELIVERY_DATE_CHR.map(
            lambda t: pd.to_datetime(t[:datenum]))
    return df


if __name__ == '__main__':
    f_data = '/Users/luochen/Documents/jiachen/baosteel/baosteel_data/baosteel_data.csv'
    df = load(f_data)
    print(df.head())

    df.to_csv('/Users/luochen/Documents/jiachen/baosteel/baosteel_data/baosteel_data_fixed.csv')

    print ('done')
