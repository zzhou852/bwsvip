import pandas as pd
import numpy as np
import datetime as dt

def process(df):
    df = df.sort_values(by=['date'])
    idx = df['value'].last_valid_index()
    ret = df.loc[idx, 'date']
    return ret

def process1(df):
    df['value'] = df['value'].drop_duplicates()
    return df['value'].mean()

def steelunion_check(ipath):
    df = pd.read_csv(ipath, names=['date', 'item', 'value'])
    df = df[df['value'].isnull() == False].reset_index(drop=True)
    
    N_item = len(df) - df['item'].duplicated().sum()
       
    results = pd.DataFrame(np.zeros([N_item, 5]), columns=['cols', 'last_update_date', 'since_today', 'last_update_value', 'skewness'])
    #print (results)
    
    print ('----- adding cols -----')
    cols = df['item'].drop_duplicates()
    cols = cols.reset_index(drop=True)
    results['cols'] = cols
    #print (results)
    
    print ('----- adding last update date -----')
    last_update_date = df[['date', 'item', 'value']].groupby('item', sort=False).apply(process)
    
    last_update_date = last_update_date.reset_index(drop=True)
    results['last_update_date'] = last_update_date
    #print (results)

    print ('----- adding since today -----')
    results['last_update_date'] = pd.to_datetime(results['last_update_date'])
    since_today = pd.to_datetime('today') - results['last_update_date']
    since_today = since_today.dt.days
    results['since_today'] = since_today
    #print (results)
    
    print ('----- adding last update value -----')
    results['last_update_date'] = results['last_update_date'].dt.strftime('%Y-%m-%d')
    tmp = pd.merge(df, results, left_on = ['item', 'date'], right_on = ['cols', 'last_update_date'])
    results['last_update_value'] = tmp['value']
    #print (results)

    print ('----- adding skewness -----')
    results['last_update_date'] = pd.to_datetime(results['last_update_date'])
    lens = df[['item', 'date']].groupby('item', sort=False).count()
    lens = np.array(lens).flatten()
    l_bound = results['last_update_date'] - pd.Timedelta(days=365)
    l_bound = l_bound.dt.strftime('%Y-%m-%d')
    mask = np.repeat(np.array(l_bound), lens, axis=0)
    mask = pd.Series(mask, name='date') 
    df['mask'] = df['date'] >= mask
    
    last_year_mean = df[['item','value']][df['mask']].groupby('item', sort=False).apply(process1)
    last_year_mean = last_year_mean.reset_index(drop=True)
    results['skewness'] = results['last_update_value'] / last_year_mean
    #print (results)
    
    results.to_csv('./output/steelunion_check.data', index=False)
    print ('done')
    
if __name__ == '__main__':
    ipath = '/home/luochen/baogang/check/temp.mysteel_hive.csv'
    #ipath = '/home/luochen/baogang/check/test3.data'

    steelunion_check(ipath)

