import numpy as np
import pandas as pd
import datetime as dt

def process(df):
    df = df.sort_values(by=['DATE'])
    idx = df['INNUM_SUM'].last_valid_index()
    ret = df.loc[idx, 'DATE']
    return ret 

def process1(df, itm):
    df = df.sort_values(by=['DATE'])
    idx = df[itm].last_valid_index()
    ret = df.loc[idx, 'DATE']
    return ret

def storage_check(ipath):
    print ('deleting index')
    df = pd.read_csv(ipath)
    del df['Unnamed: 0']
    
    print ('-----getting CATATYPE SHOPSIGN ADDR-----')
    key = df[['CATATYPE', 'SHOPSIGN', 'ADDR']].drop_duplicates()
    key = key.reset_index(drop=True)
    results = key
    #print (results)

    print ('-----preprocessing-----')
    tmp_sum = df[['ADDR', 'CATATYPE', 'SHOPSIGN', 'DATE', 'INNUM', 'OUTNUM']].groupby(['ADDR', 'CATATYPE', 'SHOPSIGN', 'DATE']).sum()
    tmp_sum.reset_index(inplace=True)
    tmp_sum = tmp_sum.rename(columns={'INNUM':'INNUM_SUM','OUTNUM':'OUTNUM_SUM'})
    df_new = pd.merge(key, tmp_sum, on=['ADDR','CATATYPE', 'SHOPSIGN'])
    #print (df_new)
    
    tmp_mean = df[['ADDR', 'CATATYPE', 'SHOPSIGN', 'DATE', 'STOCK']].groupby(['ADDR', 'CATATYPE', 'SHOPSIGN', 'DATE']).mean()
    tmp_mean.reset_index(inplace=True)
    tmp_mean = tmp_mean.rename(columns={'STOCK':'STOCK_MEAN'})
    df_new = pd.merge(df_new, tmp_mean, on=['ADDR','CATATYPE', 'SHOPSIGN', 'DATE'])
    #print (df_new)
    
    tmp_std = df[['ADDR', 'CATATYPE', 'SHOPSIGN', 'DATE','STOCK']].groupby(['ADDR', 'CATATYPE', 'SHOPSIGN', 'DATE']).std(ddof=0)
    tmp_std.reset_index(inplace=True)
    tmp_std = tmp_std.rename(columns={'STOCK':'STOCK_STD'})
    df_new = pd.merge(df_new, tmp_std, on=['ADDR','CATATYPE', 'SHOPSIGN', 'DATE'])
    #print (df_new)
    
    name = ['CATATYPE','SHOPSIGN','ADDR','LAST_UPDATE_DATE','LAST_UPDATE_INNUM','LAST_UPDATE_INNUM_SKEW','LAST_UPDATE_OUTNUM','LAST_UPDATE_OUTNUM_SKEW','LAST_UPDATE_STOCK_MEAN', 'LAST_UPDATE_STOCK_STD', 'LAST_UPDATE_STOCK_SKEW']
    
    print ('-----last update date-----')
    last_update_date = df_new[['CATATYPE','SHOPSIGN','ADDR', 'DATE', 'INNUM_SUM']].groupby(['CATATYPE','SHOPSIGN','ADDR']).apply(process)
    last_update_date = last_update_date.reset_index()
    last_update_date = last_update_date.rename(columns={0:'LAST_UPDATE_DATE'})
    results = pd.merge(results, last_update_date, on=['CATATYPE','SHOPSIGN','ADDR'])
    #print (results)
    
    print ('-----since today-----')
    since_today = pd.to_datetime('today') - pd.to_datetime(results['LAST_UPDATE_DATE'])
    results['since_today'] = since_today.dt.days
    #print (results)
    
    print ('-----last innum sum-----')
    last_innum_sum = pd.merge(df_new[['CATATYPE','SHOPSIGN','ADDR', 'DATE','INNUM_SUM']], last_update_date, left_on=['CATATYPE','SHOPSIGN','ADDR','DATE'], right_on=['CATATYPE','SHOPSIGN','ADDR', 'LAST_UPDATE_DATE'])
    last_innum_sum = last_innum_sum.rename(columns={'INNUM_SUM':'LAST_INNUM_SUM'})
    del last_innum_sum['DATE']
    results = pd.merge(results, last_innum_sum, on=['CATATYPE','SHOPSIGN','ADDR', 'LAST_UPDATE_DATE'])
    #print (results)
    
    print ('-----last innum skew-----')
    l_bound = pd.to_datetime(results['LAST_UPDATE_DATE']) - pd.Timedelta(days=365)
    lens = df_new[['CATATYPE','SHOPSIGN','ADDR', 'DATE']].groupby(['CATATYPE','SHOPSIGN','ADDR']).count()
    lens = np.array(lens).flatten()
    mask = np.repeat(np.array(l_bound), lens, axis=0)
    mask = pd.Series(mask, name='date')
    df_new['mask'] = pd.to_datetime(df_new['DATE']) >= mask
    
    last_year_mean = df_new[['CATATYPE','SHOPSIGN','ADDR', 'INNUM_SUM']][df_new['mask']].groupby(['CATATYPE','SHOPSIGN','ADDR']).mean()
    last_year_mean = last_year_mean.reset_index()
    last_year_mean = last_year_mean.rename(columns={'INNUM_SUM':'INNUM_SUM_MEAN'})
    results = pd.merge(results, last_year_mean, on = ['CATATYPE','SHOPSIGN','ADDR'])
    results['LAST_INNUM_SKEW'] = results['LAST_INNUM_SUM'] / results['INNUM_SUM_MEAN']
    del results['INNUM_SUM_MEAN']
    #print (results)

    #print ('-----last outnum date-----')
    #last_outnum_date = df_new[['CATATYPE','SHOPSIGN','ADDR', 'DATE', 'OUTNUM_SUM']].groupby(['CATATYPE','SHOPSIGN','ADDR']).apply(lambda x:process1(x, 'OUTNUM_SUM'))
    #last_outnum_date = last_outnum_date.reset_index()
    #last_outnum_date = last_outnum_date.rename(columns={0:'LAST_OUTNUM_DATE'})
    #print (last_outnum_date)
    #results = pd.merge(results, last_outnum_date, on=['CATATYPE','SHOPSIGN','ADDR'])
    #print (results)
    
    print ('-----last outnum sum-----')
    last_outnum_sum = pd.merge(df_new[['CATATYPE','SHOPSIGN','ADDR', 'DATE','OUTNUM_SUM']], last_update_date, left_on=['CATATYPE','SHOPSIGN','ADDR','DATE'], right_on=['CATATYPE','SHOPSIGN','ADDR', 'LAST_UPDATE_DATE'])
    last_outnum_sum = last_outnum_sum.rename(columns={'OUTNUM_SUM':'LAST_OUTNUM_SUM'})
    del last_outnum_sum['DATE']
    results = pd.merge(results, last_outnum_sum, on=['CATATYPE','SHOPSIGN','ADDR', 'LAST_UPDATE_DATE'])
    #print (results)
    
    print ('-----last outnum skew-----')
    l_bound = pd.to_datetime(results['LAST_UPDATE_DATE']) - pd.Timedelta(days=365)
    lens = df_new[['CATATYPE','SHOPSIGN','ADDR', 'DATE']].groupby(['CATATYPE','SHOPSIGN','ADDR']).count()
    lens = np.array(lens).flatten()
    mask = np.repeat(np.array(l_bound), lens, axis=0)
    mask = pd.Series(mask, name='date')
    df_new['mask'] = pd.to_datetime(df_new['DATE']) >= mask
    
    last_year_mean = df_new[['CATATYPE','SHOPSIGN','ADDR', 'OUTNUM_SUM']][df_new['mask']].groupby(['CATATYPE','SHOPSIGN','ADDR']).mean()
    last_year_mean = last_year_mean.reset_index()
    last_year_mean = last_year_mean.rename(columns={'OUTNUM_SUM':'OUTNUM_SUM_MEAN'})
    results = pd.merge(results, last_year_mean, on = ['CATATYPE','SHOPSIGN','ADDR'])
    results['LAST_OUTNUM_SKEW'] = results['LAST_OUTNUM_SUM'] / results['OUTNUM_SUM_MEAN']
    del results['OUTNUM_SUM_MEAN']
    #print (results)

    #print ('-----last stock date-----')
    #last_stock_date = df_new[['CATATYPE','SHOPSIGN','ADDR', 'DATE', 'STOCK_MEAN']].groupby(['CATATYPE','SHOPSIGN','ADDR']).apply(lambda x:process1(x, 'STOCK_MEAN'))
    #last_stock_date = last_stock_date.reset_index()
    #last_stock_date = last_stock_date.rename(columns={0:'LAST_STOCK_DATE'})
    #results = pd.merge(results, last_stock_date, on=['CATATYPE','SHOPSIGN','ADDR'])
    #print (results)
    
    print ('-----last stock mean-----')
    last_stock_mean = pd.merge(df_new[['CATATYPE','SHOPSIGN','ADDR', 'DATE','STOCK_MEAN']], last_update_date, left_on=['CATATYPE','SHOPSIGN','ADDR','DATE'], right_on=['CATATYPE','SHOPSIGN','ADDR', 'LAST_UPDATE_DATE'])
    last_stock_mean = last_stock_mean.rename(columns={'STOCK_MEAN':'LAST_STOCK_MEAN'})
    del last_stock_mean['DATE']
    results = pd.merge(results, last_stock_mean, on=['CATATYPE','SHOPSIGN','ADDR', 'LAST_UPDATE_DATE'])
    #print (results)
    
    print ('-----last stock std-----')
    last_stock_std = pd.merge(df_new[['CATATYPE','SHOPSIGN','ADDR', 'DATE','STOCK_STD']], last_update_date, left_on=['CATATYPE','SHOPSIGN','ADDR','DATE'], right_on=['CATATYPE','SHOPSIGN','ADDR', 'LAST_UPDATE_DATE'])
    last_stock_std = last_stock_std.rename(columns={'STOCK_STD':'LAST_STOCK_STD'})
    del last_stock_std['DATE']
    results = pd.merge(results, last_stock_std, on=['CATATYPE','SHOPSIGN','ADDR', 'LAST_UPDATE_DATE'])
    #print (results)

    print ('-----last stock mean skew-----')
    l_bound = pd.to_datetime(results['LAST_UPDATE_DATE']) - pd.Timedelta(days=365)
    lens = df_new[['CATATYPE','SHOPSIGN','ADDR', 'DATE']].groupby(['CATATYPE','SHOPSIGN','ADDR']).count()
    lens = np.array(lens).flatten()
    mask = np.repeat(np.array(l_bound), lens, axis=0)
    mask = pd.Series(mask, name='date')
    df_new['mask'] = pd.to_datetime(df_new['DATE']) >= mask
    
    last_year_mean = df_new[['CATATYPE','SHOPSIGN','ADDR', 'STOCK_MEAN']][df_new['mask']].groupby(['CATATYPE','SHOPSIGN','ADDR']).mean()
    last_year_mean = last_year_mean.reset_index()
    last_year_mean = last_year_mean.rename(columns={'STOCK_MEAN':'LAST_YEAR_STOCK_MEAN'})
    results = pd.merge(results, last_year_mean, on = ['CATATYPE','SHOPSIGN','ADDR'])
    results['LAST_STOCK_MEAN_SKEW'] = results['LAST_STOCK_MEAN'] / results['LAST_YEAR_STOCK_MEAN']
    del results['LAST_YEAR_STOCK_MEAN']
    results.to_csv('./output/storage_check.data', index='False')
    #print (results)


if __name__ == '__main__':
    ipath = '../check/temp.stock.csv'

    storage_check(ipath)
